package ru.intervi.oneapril;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Random;

public class SpawnItemTask extends BukkitRunnable {
    private final Main MAIN;
    private Random random = new Random();

    public SpawnItemTask(Main main) {
        MAIN = main;
    }

    @Override
    public void run() {
        if (MAIN.CONFIG.spawnFakeItemPercent <= 0) return;
        for (Player player : Bukkit.getOnlinePlayers()) {
            Location loc = player.getLocation();
            if (random.nextInt(100) + 1 <= MAIN.CONFIG.spawnFakeItemPercent) {
                Location spawnLoc = MAIN.getRandomLocation(loc, MAIN.CONFIG.spawnRadius);
                loc.getWorld().dropItemNaturally(spawnLoc, MAIN.getRandomFakeItem());
            }
        }
    }
}
