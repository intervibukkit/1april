package ru.intervi.oneapril;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Config {
    private final Main MAIN;

    public HashMap<String, List<String>> cmdMap = new HashMap<>();
    public HashMap<String, String> repMap = new HashMap<>();
    public ArrayList<Material> fakeItems = new ArrayList<>();
    public ArrayList<Sound> soundList = new ArrayList<>();
    public int fallPercent;
    public int breakExplosionPercent;
    public int breakFakeItemsPercent;
    public String magicItemName;
    public int spawnTaskTick;
    public int spawnFakeItemPercent;
    public int spawnRadius;
    public int playSoundPercent;
    public int soundTaskTick;
    public int soundRadius;
    public int soundVolume;

    public Config(Main main) {
        MAIN = main;
    }

    public void load() {
        cmdMap.clear();
        repMap.clear();
        fakeItems.clear();
        soundList.clear();
        MAIN.saveDefaultConfig();
        MAIN.reloadConfig();
        FileConfiguration conf = MAIN.getConfig();
        if (conf.isConfigurationSection("commands")) {
            ConfigurationSection cmdSec = conf.getConfigurationSection("commands");
            for (String key : cmdSec.getKeys(false)) {
                cmdMap.put(key.toLowerCase(), cmdSec.getStringList(key));
            }
        }
        for (String str : conf.getStringList("replace")) {
            String[] value = str.split("\\:");
            repMap.put(value[0].trim(), value[1].trim());
        }
        for (String str : conf.getStringList("fakeItems")) {
            fakeItems.add(Material.valueOf(str.toUpperCase()));
        }
        for (String str : conf.getStringList("sounds")) {
            soundList.add(Sound.valueOf(str.toUpperCase()));
        }
        fallPercent = conf.getInt("fallPercent");
        breakExplosionPercent = conf.getInt("breakExplosionPercent");
        breakFakeItemsPercent = conf.getInt("breakFakeItemsPercent");
        magicItemName = ChatColor.translateAlternateColorCodes(
                '&', conf.getString("magicItemName")
        );
        spawnTaskTick = conf.getInt("spawnTaskTick");
        spawnFakeItemPercent = conf.getInt("spawnFakeItemPercent");
        spawnRadius = conf.getInt("spawnRadius");
        playSoundPercent = conf.getInt("playSoundPercent");
        soundTaskTick = conf.getInt("soundTaskTick");
        soundRadius = conf.getInt("soundRadius");
        soundVolume = conf.getInt("soundVolume");
    }
}
