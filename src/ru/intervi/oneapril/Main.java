package ru.intervi.oneapril;

import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.inventory.InventoryPickupItemEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Map;
import java.util.Random;

public class Main extends JavaPlugin implements Listener {
    final Config CONFIG = new Config(this);
    private Random random = new Random();
    private SpawnItemTask spawnTask = new SpawnItemTask(this);
    private PlaySoundTask soundTask = new PlaySoundTask(this);

    @Override
    public void onEnable() {
        CONFIG.load();
        startTasks();
        getServer().getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {
        stopTasks();
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onCommand(PlayerCommandPreprocessEvent event) {
        if (CONFIG.cmdMap.isEmpty()) return;
        Player player = event.getPlayer();
        if (player.hasPermission("1april.exempt")) return;
        if (event.getMessage() == null || event.getMessage().length() < 2) return;
        String[] command = event.getMessage().substring(1).split(" ");
        if (!CONFIG.cmdMap.containsKey(command[0].toLowerCase())) return;
        for (String line : CONFIG.cmdMap.get(command[0].toLowerCase())) {
            for (int i = 1; i < command.length; i++) {
                line = line.replace("{" + i + "}", command[i]);
            }
            line = line.replace("{name}", player.getDisplayName());
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', line));
        }
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void onChat(AsyncPlayerChatEvent event) {
        if (CONFIG.repMap.isEmpty()) return;
        if (event.getPlayer().hasPermission("1april.exempt")) return;
        String msg = event.getMessage();
        for (Map.Entry<String, String> entry : CONFIG.repMap.entrySet()) {
            msg = msg.replace(entry.getKey(), entry.getValue());
        }
        event.setMessage(msg);
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlace(BlockPlaceEvent event) {
        if (CONFIG.fallPercent <= 0) return;
        if (event.getPlayer().hasPermission("1april.exempt")) return;
        if (event.getBlock().getState() instanceof InventoryHolder) return;
        if (random.nextInt(100) + 1 <= CONFIG.fallPercent) {
            Location loc = event.getBlock().getLocation();
            loc.getWorld().spawnFallingBlock(loc, event.getBlock().getState().getData());
            event.setCancelled(true);
            if (event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) return;
            if (event.getHand().equals(EquipmentSlot.HAND)) {
                ItemStack item = event.getPlayer().getInventory().getItemInMainHand();
                item.setAmount(item.getAmount() - 1);
                if (item.getAmount() <= 0) item = new ItemStack(Material.AIR);
                event.getPlayer().getInventory().setItemInMainHand(item);
            } else {
                ItemStack item = event.getPlayer().getInventory().getItemInOffHand();
                item.setAmount(item.getAmount() - 1);
                if (item.getAmount() <= 0) item = new ItemStack(Material.AIR);
                event.getPlayer().getInventory().setItemInOffHand(item);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBreak(BlockBreakEvent event) {
        if (CONFIG.breakExplosionPercent <= 0) return;
        if (event.getPlayer().hasPermission("1april.exempt")) return;
        Location loc = event.getBlock().getLocation();
        if (random.nextInt(100) + 1 <= CONFIG.breakExplosionPercent) {
            if (!(event.getBlock().getState() instanceof InventoryHolder)) {
                loc.getWorld().createExplosion(
                        loc.getX(), loc.getY(), loc.getZ(), 1, false, false
                );
            }
        }
        if (random.nextInt(100) + 1 <= CONFIG.breakFakeItemsPercent) {
            loc.getWorld().dropItemNaturally(loc, getRandomFakeItem());
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPickup(PlayerPickupItemEvent event) {
        if (!isMagicItem(event.getItem().getItemStack())) return;
        event.setCancelled(true);
        event.getItem().remove();
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onEntityPickup(EntityPickupItemEvent event) {
        if (!isMagicItem(event.getItem().getItemStack())) return;
        event.setCancelled(true);
        event.getItem().remove();
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onInvPickup(InventoryPickupItemEvent event) {
        if (!isMagicItem(event.getItem().getItemStack())) return;
        event.setCancelled(true);
        event.getItem().remove();
    }

    private boolean isMagicItem(ItemStack item) {
        if (!item.hasItemMeta() || !item.getItemMeta().hasDisplayName() ||
                !item.getItemMeta().getDisplayName().equals(CONFIG.magicItemName)) return false;
        return true;
    }

    public ItemStack getRandomFakeItem() {
        ItemStack item = new ItemStack(CONFIG.fakeItems.get(random.nextInt(CONFIG.fakeItems.size())));
        item.setAmount(random.nextInt(item.getMaxStackSize()));
        ItemMeta meta;
        if (item.hasItemMeta()) meta = item.getItemMeta();
        else {
            meta = Bukkit.getItemFactory().getItemMeta(item.getType());
        }
        meta.setDisplayName(CONFIG.magicItemName);
        item.setItemMeta(meta);
        return item;
    }

    public Location getRandomLocation(Location loc, int rad) {
        int x = loc.getBlockX() + (random.nextInt(rad * 2) + 1) - rad - 1;
        int y = loc.getBlockY() + (random.nextInt(rad * 2) + 1);
        int z = loc.getBlockZ() + (random.nextInt(rad * 2) + 1) - rad - 1;
        return new Location(loc.getWorld(), x, y, z);
    }

    private void startTasks() {
        if (CONFIG.spawnTaskTick > 0)
            spawnTask.runTaskTimer(this, CONFIG.spawnTaskTick, CONFIG.spawnTaskTick);
        if (CONFIG.soundTaskTick > 0)
            soundTask.runTaskTimer(this, CONFIG.soundTaskTick, CONFIG.soundTaskTick);
    }

    private void stopTasks() {
        spawnTask.cancel();
        soundTask.cancel();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("1april.reload") && !(sender instanceof ConsoleCommandSender)) {
            sender.sendMessage("no permission");
            return true;
        }
        CONFIG.load();
        sender.sendMessage("config reloaded");
        return true;
    }
}
