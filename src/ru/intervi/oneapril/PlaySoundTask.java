package ru.intervi.oneapril;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Random;

public class PlaySoundTask extends BukkitRunnable {
    private final Main MAIN;
    private Random random = new Random();

    public PlaySoundTask(Main main) {
        MAIN = main;
    }

    @Override
    public void run() {
        if (MAIN.CONFIG.playSoundPercent <= 0) return;
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (random.nextInt(100) + 1 <= MAIN.CONFIG.playSoundPercent) {
                Sound sound = MAIN.CONFIG.soundList.get(random.nextInt(MAIN.CONFIG.soundList.size()));
                Location loc = MAIN.getRandomLocation(player.getLocation(), MAIN.CONFIG.soundRadius);
                loc.getWorld().playSound(
                        loc, sound,
                        MAIN.CONFIG.soundVolume,
                        player.getLocation().getPitch()
                );
            }
        }
    }
}
